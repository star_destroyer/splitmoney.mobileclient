﻿using Autofac;

namespace SplitMoney.MobileClient
{
    public static class AppContainer
    {
        public static IContainer Container { get; set; }
    }
}