using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Conversations.Contracts
{
    public class ConversationList
    {
        public Conversation[] Conversations { get; set; }
    }
}