using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Conversations.Contracts;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public class ConversationClient : IConversationClient
    {
        private readonly IClientSender sender;

        public ConversationClient(IClientSender sender)
        {
            this.sender = sender;
        }

        public Task<ServerInteractionInfo<ConversationList>> GetConversations(CancellationToken cancellation = default(CancellationToken))
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<ConversationList>(GetListConversationMessage(), cancellation);
        }

        public Task<ServerInteractionInfo<Conversation>> CreateConversation(IEnumerable<string> participants, string name = null, CancellationToken cancellation = default(CancellationToken))
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<Conversation>(GetCreateConversationMessage(participants, name), cancellation);
        }

        private static HttpRequestMessage GetListConversationMessage()
        {
            return new RequestBuilder(HttpMethod.Get, "/conversations/list")
                .WithAuthorization()
                .Build();
        }

        private static HttpRequestMessage GetCreateConversationMessage(IEnumerable<string> participants, string name)
        {
            return new RequestBuilder(HttpMethod.Post, "/conversations/createOrUpdate")
                .WithAuthorization()
                .WithJsonContent(new { Name = name, Participants = participants })
                .Build();
        }
    }
}