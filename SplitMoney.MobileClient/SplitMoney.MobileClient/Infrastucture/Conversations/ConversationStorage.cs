using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public class ConversationStorage : IConversationStorage
    {
        private Dictionary<Guid, Conversation> storedConversations = new Dictionary<Guid, Conversation>();

        public Task<Conversation> Get(Guid id, CancellationToken cancellation)
        {
            return Task.FromResult(storedConversations.ContainsKey(id) ? storedConversations[id] : null);
        }

        public Task<Conversation[]> Select(CancellationToken cancellation)
        {
            return Task.FromResult(storedConversations.Values.ToArray());
        }

        public async Task Write(Guid id, Conversation conversation, CancellationToken cancellation)
        {
            storedConversations[id] = conversation;
        }

        public async Task Write(Conversation[] conversations, CancellationToken cancellation)
        {
            storedConversations = conversations.ToDictionary(x => x.Id);
        }
    }
}