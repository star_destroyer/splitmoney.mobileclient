using System;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public interface IConversationStorage
    {
        Task<Conversation> Get(Guid id, CancellationToken cancellation);

        Task<Conversation[]> Select(CancellationToken cancellation);

        Task Write(Guid id, Conversation conversation, CancellationToken cancellation);

        Task Write(Conversation[] conversations, CancellationToken cancellation);
    }
}