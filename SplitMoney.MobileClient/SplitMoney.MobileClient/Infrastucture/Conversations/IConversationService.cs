using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public interface IConversationService
    {
        Task<bool> RefreshConversation(CancellationToken cancellation);

        Task<ServerInteractionInfo<Conversation>> CreateNewConversation(string phone, string name, CancellationToken cancellation);
    }
}