using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Conversations.Contracts;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public interface IConversationClient
    {
        Task<ServerInteractionInfo<ConversationList>> GetConversations(CancellationToken cancellation = default (CancellationToken));

        Task<ServerInteractionInfo<Conversation>> CreateConversation(IEnumerable<string> participants, string name = null, CancellationToken cancellation = default(CancellationToken));
    }
}