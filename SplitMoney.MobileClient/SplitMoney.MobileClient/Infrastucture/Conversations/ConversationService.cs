﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Conversations
{
    public class ConversationService : IConversationService
    {
        private readonly IConversationStorage conversationStorage;
        private readonly IConversationClient conversationClient;

        public ConversationService(IConversationStorage conversationStorage, IConversationClient conversationClient)
        {
            this.conversationStorage = conversationStorage;
            this.conversationClient = conversationClient;
        }

        public async Task<bool> RefreshConversation(CancellationToken cancellation)
        {
            var info = await conversationClient.GetConversations(cancellation);
            if (!info.IsSuccess)
            {
                return false;
            }

            await conversationStorage.Write(info.Body.Conversations, cancellation);
            return true;
        }

        public async Task<ServerInteractionInfo<Conversation>> CreateNewConversation(string phone, string name, CancellationToken cancellation)
        {
            var info = await conversationClient.CreateConversation(new []{phone}, name, cancellation);
            if (info.IsSuccess)
            {
                await conversationStorage.Write(info.Body.Id, info.Body, cancellation);
            }
            return info;
        }
    }
}