using System.Linq;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Pages;
using SplitMoney.MobileClient.ViewModels;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.Infrastucture.Core
{
    public class NavigationService : INavigationService
    {
        public MasterPage AsMaster => Application.Current.MainPage as MasterPage;
        
        public async Task GoTo(Page page, params object[] parameters)
        {
            await Application.Current.MainPage.Navigation.PushAsync(page);
            var withViewModel = page as IWithViewModel;
            if (withViewModel != null)
                await withViewModel.ViewModel.Init(parameters);
        }

        public async Task GoToInMasterStack(Page page, params object[] parameters)
        {
            await AsMaster.Detail.Navigation.PushAsync(page);
            var withViewModel = page as IWithViewModel;
            if (withViewModel != null)
                await withViewModel.ViewModel.Init(parameters);
        }

        public async Task GoToInNewStak(Page page, params object[] parameters)
        {
            Application.Current.MainPage = new NavigationPage(page);
            var withViewModel = page as IWithViewModel;
            if (withViewModel != null)
                await withViewModel.ViewModel.Init(parameters);
        }

        public async Task GoToInNewMasterStak(Page page, params object[] parameters)
        {
            Application.Current.MainPage = new MasterPage(page);
            var withViewModel = page as IWithViewModel;
            if (withViewModel != null)
                await withViewModel.ViewModel.Init(parameters);
        }

        public async Task Back(params object[] parameters)
        {
            await Application.Current.MainPage.Navigation.PopAsync();
            var page = AsMaster.Detail.Navigation.NavigationStack.Last();
            var withViewModel = page as IWithViewModel;
            withViewModel?.ViewModel.Refresh(parameters);
        }

        public async Task BackInMaster(params object[] parameters)
        {
            await AsMaster.Detail.Navigation.PopAsync();
            var page = AsMaster.Detail.Navigation.NavigationStack.Last();
            var withViewModel = page as IWithViewModel;
            withViewModel?.ViewModel.Refresh(parameters);
        }
    }
}