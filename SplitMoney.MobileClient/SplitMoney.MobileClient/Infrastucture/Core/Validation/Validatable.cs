using System.Collections.Generic;
using SplitMoney.MobileClient.ViewModels;

namespace SplitMoney.MobileClient.Infrastucture.Core.Validation
{
    public class Validatable<TValue> : Notificatable
    {
        public TValue Value
        {
            get => Get<TValue>();
            set => Set(value);
        }

        public bool IsInvalid
        {
            get => Get(false);
            set => Set(value);
        }

        public string ValidationMessage
        {
            get => Get<string>();
            set => Set(value);
        }

        public List<ValidationRule<TValue>> ValidationRules { get; } = new List<ValidationRule<TValue>>();

        public bool CheckValidation()
        {
            foreach (var validationRule in ValidationRules)
            {
                if (validationRule.IsInvalid)
                {
                    IsInvalid = true;
                    ValidationMessage = validationRule.Message;
                    return false;
                }
            }
            IsInvalid = false;
            ValidationMessage = null;
            return true;
        }

        public void Invalidate(string message)
        {
            IsInvalid = true;
            ValidationMessage = message;
        }
    }
}