using System;

namespace SplitMoney.MobileClient.Infrastucture.Core.Validation
{
    public class ValidationBuilder<TValue>
    {
        private readonly Validatable<TValue> validatable;

        public ValidationBuilder(Validatable<TValue> validatable)
        {
            this.validatable = validatable;
        }

        public ValidationBuilder<TValue> AddRule(Func<bool> invalidationFunc, string message)
        {
            validatable.ValidationRules.Add(new ValidationRule<TValue>(invalidationFunc, message));
            return this;
        }
    }
}