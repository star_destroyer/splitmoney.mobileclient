﻿using System;

namespace SplitMoney.MobileClient.Infrastucture.Core.Validation
{
    public class ValidationRule<TValue>
    {
        private readonly Func<bool> invalidationFunc;

        public ValidationRule(Func<bool> invalidationFunc, string message)
        {
            this.invalidationFunc = invalidationFunc;
            Message = message;
        }

        public bool IsInvalid => invalidationFunc.Invoke();

        public string Message { get; }
    }
}