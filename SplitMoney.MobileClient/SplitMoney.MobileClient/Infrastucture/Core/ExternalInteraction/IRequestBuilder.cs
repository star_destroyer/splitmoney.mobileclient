using System.Net.Http;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public interface IRequestBuilder
    {
        IRequestBuilder WithAuthorization();

        IRequestBuilder WithContent(HttpContent content);

        IRequestBuilder WithJsonContent(string content);

        IRequestBuilder WithJsonContent(object content);

        HttpRequestMessage Build();
    }
}