namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public class ServerInteractionInfo<TBody> where TBody : class 
    {
        public bool IsSuccess => Result == ServerInteractionResult.Success;

        public ServerInteractionResult Result { get; set; }

        public TBody Body { get; set; }
    }
}