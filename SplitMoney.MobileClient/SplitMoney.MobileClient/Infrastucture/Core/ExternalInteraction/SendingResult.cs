using System.Net;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public class SendingResult
    {
        public SendingResult(bool isSendSuccess, bool isCodeSuccess, HttpStatusCode code, string body = null)
        {
            IsSendSuccess = isSendSuccess;
            IsCodeSuccess = isCodeSuccess;
            Code = code;
            Body = body;
        }

        public bool IsSendSuccess { get; }

        public bool IsCodeSuccess { get; }

        public HttpStatusCode Code { get; }

        public string Body { get; }
    }
}