﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Autofac;
using Newtonsoft.Json;
using SplitMoney.MobileClient.Infrastucture.Auth;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public class RequestBuilder : IRequestBuilder
    {
        private static readonly IApiUriProvider ApiUriProvider = AppContainer.Container.Resolve<IApiUriProvider>();
        private static readonly IAuthorizationStorage AuthorizationStorage = AppContainer.Container.Resolve<IAuthorizationStorage>();

        private readonly HttpRequestMessage requestMessage;

        public RequestBuilder(HttpMethod method, string uri)
        {
            requestMessage = new HttpRequestMessage(method, ApiUriProvider.GetRequestUri(uri));
            requestMessage.Headers.Accept.ParseAdd("application/json");
        }

        public IRequestBuilder WithAuthorization()
        {
            var info = AuthorizationStorage.Get(CancellationToken.None).GetAwaiter().GetResult();
            if (info?.AccessToken == null || info?.Phone == null)
            {
                throw new ArgumentException("Пользователь еще не авторизован");
            }
            var auth = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{info.Phone}:{info.AccessToken}"));
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Token", auth);
            return this;
        }

        public IRequestBuilder WithContent(HttpContent content)
        {
            requestMessage.Content = content;
            return this;
        }

        public IRequestBuilder WithJsonContent(string content)
        {
            return WithContent(new StringContent(content, Encoding.UTF8, "application/json"));
        }

        public IRequestBuilder WithJsonContent(object content)
        {
            var json = JsonConvert.SerializeObject(content);
            return WithJsonContent(json);
        }

        public HttpRequestMessage Build()
        {
            return requestMessage;
        }
    }
}