﻿using System;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
	public interface IApiUriProvider
	{
		Uri BaseApiUri { get; }

	    Uri GetRequestUri(string uri);
	}
}
