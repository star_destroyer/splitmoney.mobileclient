using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
	public class ClientSender : IClientSender
    {
        private HttpClient client;
        private HttpClient Client => client ?? (client = new HttpClient {Timeout = TimeSpan.FromSeconds(10)});

        public async Task<ServerInteractionInfo<TBody>> SendAsync<TBody>(HttpRequestMessage request, CancellationToken cancellation) where TBody : class
        {
            try
            {
                var response = await Client.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(true);
                if (response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    return new ServerInteractionInfo<TBody> {Result = ServerInteractionResult.ServerError};
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return new ServerInteractionInfo<TBody> {Result = ServerInteractionResult.Unauthorized};
                }
                else if(!response.IsSuccessStatusCode)
                {
                    return new ServerInteractionInfo<TBody> {Result = ServerInteractionResult.BadParameters};
                }

                var content = await response.Content.ReadAsStringAsync();
                var body = JsonConvert.DeserializeObject<TBody>(content);
                return new ServerInteractionInfo<TBody> {Result = ServerInteractionResult.Success, Body = body};
            }
            catch (Exception e)
            {
                return new ServerInteractionInfo<TBody> {Result = ServerInteractionResult.NoConnection};
            }
        }
    }
}