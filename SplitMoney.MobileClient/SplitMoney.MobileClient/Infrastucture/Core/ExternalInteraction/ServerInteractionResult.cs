﻿namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public enum ServerInteractionResult
    {
        Success = 0,
        NoConnection = 1,
        ServerError = 2,
        BadParameters = 3,
        Unauthorized = 4
    }
}