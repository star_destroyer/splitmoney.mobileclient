using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
    public interface IClientSender
    {
        Task<ServerInteractionInfo<TBody>> SendAsync<TBody>(HttpRequestMessage request, CancellationToken cancellation) where TBody : class;
    }
}