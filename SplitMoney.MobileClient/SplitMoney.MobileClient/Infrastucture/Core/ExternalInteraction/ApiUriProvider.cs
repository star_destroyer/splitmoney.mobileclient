﻿using System;

namespace SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction
{
	public class ApiUriProvider : IApiUriProvider
	{
		public Uri BaseApiUri => new Uri("http://splitmoney-master.northeurope.cloudapp.azure.com:80/");
	    public Uri GetRequestUri(string uri)
	    {
	        return new UriBuilder(BaseApiUri) {Path = uri}.Uri;
	    }
	}
}
