﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.Infrastucture.Core
{
    public interface INavigationService
    {
        Task GoTo(Page page, params object[] parameters);

        Task GoToInMasterStack(Page page, params object[] parameters);

        Task GoToInNewStak(Page page, params object[] parameters);

        Task GoToInNewMasterStak(Page page, params object[] parameters);

        Task Back(params object[] parameters);

        Task BackInMaster(params object[] parameters);
    }
}