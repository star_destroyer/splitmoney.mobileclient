﻿using System;
using System.Linq.Expressions;

namespace SplitMoney.MobileClient.Infrastucture.Core.Utils
{
    public static class ExpressionsExtensions
    {
        public static string GetPropertyName<TValue>(this Expression<Func<TValue>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            return memberExpression.Member.Name;
        }
    }
}