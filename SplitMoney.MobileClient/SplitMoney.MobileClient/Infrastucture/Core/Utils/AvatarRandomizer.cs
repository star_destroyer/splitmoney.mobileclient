﻿using System;

namespace SplitMoney.MobileClient.Infrastucture.Core.Utils
{
    public static class AvatarRandomizer
    {
        private static readonly Random Random = new Random();
        public static string Get()
        {
            var number = Random.Next(1, 7);

            return $"user{number}.png";
        }
    }
}