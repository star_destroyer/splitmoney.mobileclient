﻿using System.Text.RegularExpressions;

namespace SplitMoney.MobileClient.Infrastucture.Core.Utils
{
    public static class StringExtensions
    {
        private static readonly Regex OnlyDigitsPattern = new Regex(@"\D*", RegexOptions.IgnoreCase);

        public static string GetOnlyDigits(this string value)
        {
            return OnlyDigitsPattern.Replace(value, "");
        }
    }
}