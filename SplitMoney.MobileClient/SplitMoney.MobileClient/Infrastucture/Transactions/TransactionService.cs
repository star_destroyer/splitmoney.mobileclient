using System;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Transactions.Contracts;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionClient transactionClient;
        private readonly ITransactionStorage transactionStorage;

        public TransactionService(ITransactionClient transactionClient, ITransactionStorage transactionStorage)
        {
            this.transactionClient = transactionClient;
            this.transactionStorage = transactionStorage;
        }

        public async Task<bool> RefreshTransactions(Guid conversationId, CancellationToken cancellation)
        {
            var info = await transactionClient.GetTransactions(conversationId, cancellation);
            if (info.IsSuccess)
            {
                await transactionStorage.Write(conversationId, info.Body.Transactions, cancellation);
            }
            return info.IsSuccess;
        }

        public async Task<Transaction> CreateTransaction(Guid conversationId, decimal amount, CancellationToken cancellation)
        {
            var info = await transactionClient.Create(conversationId, amount, Currency.RUB, cancellation);
            if (!info.IsSuccess)
            {
                return null;
            }
            await transactionStorage.Write(info.Body.Transaction.Id, info.Body.Transaction, cancellation);
            return info.Body.Transaction;
        }

        public async Task<ServerInteractionInfo<TransactionInfo>> ConfirmTransaction(Guid transactionId, string smsCode, CancellationToken cancellation)
        {
            var info = await transactionClient.ConfirmCode(transactionId, smsCode, cancellation);
            if (info.IsSuccess)
            {
                await transactionStorage.Write(info.Body.Transaction.Id, info.Body.Transaction, cancellation);
            }
            return info;
        }
    }
}