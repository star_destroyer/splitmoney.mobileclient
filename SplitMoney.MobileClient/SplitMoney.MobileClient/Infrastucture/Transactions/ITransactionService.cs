using System;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Transactions.Contracts;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public interface ITransactionService
    {
        Task<bool> RefreshTransactions(Guid conversationId, CancellationToken cancellation);

        Task<Transaction> CreateTransaction(Guid conversationId, decimal amount, CancellationToken cancellation);

        Task<ServerInteractionInfo<TransactionInfo>> ConfirmTransaction(Guid transactionId, string smsCode, CancellationToken cancellation);
    }
}