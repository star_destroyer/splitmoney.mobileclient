using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public class TransactionStorage : ITransactionStorage
    {
        public Guid ConversationId { get; private set; }

        private Dictionary<Guid, Transaction> storableTransactions = new Dictionary<Guid, Transaction>();

        public Task<Transaction> Get(Guid id, CancellationToken cancellation = default(CancellationToken))
        {
            return Task.FromResult(storableTransactions.ContainsKey(id) ? storableTransactions[id] : null);
        }

        public Task<Transaction[]> Select(CancellationToken cancellation = new CancellationToken())
        {
            return Task.FromResult(storableTransactions.Values.ToArray());
        }

        public async Task Write(Guid id, Transaction transaction, CancellationToken cancellation = default(CancellationToken))
        {
            storableTransactions[id] = transaction;
        }

        public async Task Write(Guid conversationId, Transaction[] transactions, CancellationToken cancellation = default(CancellationToken))
        {
            ConversationId = conversationId;
            storableTransactions = transactions.ToDictionary(x => x.Id);
        }
    }
}