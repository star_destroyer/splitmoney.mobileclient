using System;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public interface ITransactionStorage
    {
        Guid ConversationId { get; }

        Task<Transaction> Get(Guid id, CancellationToken cancellation = default(CancellationToken));

        Task<Transaction[]> Select(CancellationToken cancellation = default(CancellationToken));

        Task Write(Guid id, Transaction transaction, CancellationToken cancellation = default(CancellationToken));

        Task Write(Guid conversationId, Transaction[] transactions, CancellationToken cancellation = default(CancellationToken));
    }
}