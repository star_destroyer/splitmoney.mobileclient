using System;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Transactions.Contracts;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public interface ITransactionClient
    {
        Task<ServerInteractionInfo<TransactionsList>> GetTransactions(Guid conversationId, CancellationToken cancellation);

        Task<ServerInteractionInfo<TransactionInfo>> GetTransaction(Guid transactionId, CancellationToken cancellation);

        Task<ServerInteractionInfo<TransactionInfo>> Create(Guid recipientConversation, decimal amount, Currency currency, CancellationToken cancellation);

        Task<ServerInteractionInfo<TransactionInfo>> ConfirmCode(Guid transactionId, string smsCode, CancellationToken cancellation);
    }
}