using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Transactions.Contracts;

namespace SplitMoney.MobileClient.Infrastucture.Transactions
{
    public class TransactionClient : ITransactionClient
    {
        private readonly IClientSender sender;

        public TransactionClient(IClientSender sender)
        {
            this.sender = sender;
        }

        public Task<ServerInteractionInfo<TransactionsList>> GetTransactions(Guid conversationId, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<TransactionsList>(GetTransactionsMessage(conversationId), cancellation);
        }

        public Task<ServerInteractionInfo<TransactionInfo>> GetTransaction(Guid transactionId, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<TransactionInfo>(GetTransactionMessage(transactionId), cancellation);
        }

        public Task<ServerInteractionInfo<TransactionInfo>> Create(Guid recipientConversation, decimal amount, Currency currency, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<TransactionInfo>(GetCreateTransactionMessage(recipientConversation, amount, currency), cancellation);
        }

        public Task<ServerInteractionInfo<TransactionInfo>> ConfirmCode(Guid transactionId, string smsCode, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<TransactionInfo>(GetConfirmCodeMessage(transactionId, smsCode), cancellation);
        }

        private static HttpRequestMessage GetTransactionsMessage(Guid conversationId)
        {
            return new RequestBuilder(HttpMethod.Post, "/transactions/list")
                .WithAuthorization()
                .WithJsonContent($@"""{conversationId}""")
                .Build();
        }

        private static HttpRequestMessage GetTransactionMessage(Guid transactionId)
        {
            return new RequestBuilder(HttpMethod.Get, "/transactions/get")
                .WithAuthorization()
                .WithJsonContent($@"""{transactionId}""")
                .Build();
        }

        private static HttpRequestMessage GetCreateTransactionMessage(Guid recipientConversation, decimal amount, Currency currency, Guid? paymentMethod = null)
        {
            return new RequestBuilder(HttpMethod.Post, "/transactions/create")
                .WithAuthorization()
                .WithJsonContent(new
                {
                    RecipientConversation = recipientConversation,
                    Amount = amount,
                    Currency = currency,
                    PaymentMethod = paymentMethod
                })
                .Build();
        }

        private HttpRequestMessage GetConfirmCodeMessage(Guid transactionId, string smsCode)
        {
            return new RequestBuilder(HttpMethod.Post, "/transactions/confirm")
                .WithAuthorization()
                .WithJsonContent(new
                {
                    TransactionId = transactionId,
                    Options = new Dictionary<string, string> {{"sms-code", smsCode}}
                })
                .Build();
        }
    }
}