using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Transactions.Contracts
{
    public class TransactionsList
    {
        public Transaction[] Transactions { get; set; }
    }
}