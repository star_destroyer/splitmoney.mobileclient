using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Transactions.Contracts
{
    public class TransactionInfo
    {
        public Transaction Transaction { get; set; }
    }
}