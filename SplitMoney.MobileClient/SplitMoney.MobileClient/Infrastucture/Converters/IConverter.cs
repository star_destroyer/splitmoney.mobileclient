namespace SplitMoney.MobileClient.Infrastucture.Converters
{
    public interface IConverter<TView, TEntity>
    {
        TEntity ToEntity(TView view);

        TView ToView(TEntity entity);
    }
}