using System;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.ViewModels.Transactions;

namespace SplitMoney.MobileClient.Infrastucture.Converters
{
    public class TransactionStatusConverter : IConverter<TransactionViewStatus, TransactionStatus>
    {
        public TransactionStatus ToEntity(TransactionViewStatus view)
        {
            switch (view)
            {
                case TransactionViewStatus.Init:
                case TransactionViewStatus.Sended:
                case TransactionViewStatus.Error:
                    return TransactionStatus.Pending;
                case TransactionViewStatus.Success:
                    return TransactionStatus.Confirmed;
                case TransactionViewStatus.Rejected:
                    return TransactionStatus.Rejected;
                default:
                    throw new ArgumentOutOfRangeException(nameof(view), view, null);
            }
        }

        public TransactionViewStatus ToView(TransactionStatus entity)
        {
            switch (entity)
            {
                case TransactionStatus.Pending:
                    return TransactionViewStatus.Init;
                case TransactionStatus.Confirmed:
                    return TransactionViewStatus.Success;
                case TransactionStatus.Rejected:
                    return TransactionViewStatus.Rejected;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity), entity, null);
            }
        }
    }
}