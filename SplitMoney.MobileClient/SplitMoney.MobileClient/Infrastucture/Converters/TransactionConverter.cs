﻿using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.ViewModels.Transactions;

namespace SplitMoney.MobileClient.Infrastucture.Converters
{
    public class TransactionConverter : IConverter<TransactionView, Transaction>
    {
        private readonly IAuthorizationStorage authorizationStorage;
        private readonly IConverter<TransactionViewType, TransactionType> typeConverter;
        private readonly IConverter<TransactionViewStatus, TransactionStatus> statusConverter;

        public TransactionConverter(IAuthorizationStorage authorizationStorage,
                                    IConverter<TransactionViewType, TransactionType> typeConverter,
                                    IConverter<TransactionViewStatus, TransactionStatus> statusConverter)
        {
            this.authorizationStorage = authorizationStorage;
            this.typeConverter = typeConverter;
            this.statusConverter = statusConverter;
        }

        public Transaction ToEntity(TransactionView view)
        {
            return new Transaction
            {
                Id = view.Id,
                Amount = view.Amount,
                ConversationId = view.ConversationId,
                Created = view.Time,
                Currency = Currency.RUB,
                From = long.Parse(view.From),
                Message = view.Comment,
                Type = typeConverter.ToEntity(view.Type),
                Status = statusConverter.ToEntity(view.Status)
            };
        }

        public TransactionView ToView(Transaction entity)
        {
            var authorizationInfo = authorizationStorage.Get().GetAwaiter().GetResult();
            return new TransactionView
            {
                Id = entity.Id,
                ConversationId = entity.ConversationId,
                Comment = entity.Message,
                Amount = entity.Amount,
                Status = statusConverter.ToView(entity.Status),
                Type = typeConverter.ToView(entity.Type),
                Time = entity.Created,
                From = entity.From.ToString(),
                IsMy = authorizationInfo.Phone.Equals(entity.From.ToString())
            };
        }
    }
}