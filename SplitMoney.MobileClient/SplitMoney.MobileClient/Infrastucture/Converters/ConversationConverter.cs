using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.ViewModels.Conversations;

namespace SplitMoney.MobileClient.Infrastucture.Converters
{
    public class ConversationConverter : IConverter<ConversationView, Conversation>
    {
        public Conversation ToEntity(ConversationView view)
        {
            return new Conversation
            {
                Id = view.Id,
                Name = view.Name,
                Balance = view.Balance,
                LastEvent = view.LastMessageTime,
                Notifications = view.NotificationCount,
                Participants = view.Participants,
                Type = ConversationType.Dialog
            };
        }

        public ConversationView ToView(Conversation entity)
        {
            return new ConversationView
            {
                Id = entity.Id,
                Name = entity.Name,
                Balance = entity.Balance,
                Icon = AvatarRandomizer.Get(),
                LastMessageTime = entity.LastEvent,
                Participants = entity.Participants
            };
        }
    }
}