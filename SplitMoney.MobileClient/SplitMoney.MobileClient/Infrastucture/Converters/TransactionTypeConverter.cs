using System;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.ViewModels.Transactions;

namespace SplitMoney.MobileClient.Infrastucture.Converters
{
    public class TransactionTypeConverter : IConverter<TransactionViewType, TransactionType>
    {
        public TransactionType ToEntity(TransactionViewType view)
        {
            switch (view)
            {
                case TransactionViewType.QiwiTransfer:
                    return TransactionType.QiwiTransfer;
                case TransactionViewType.Cash:
                    return TransactionType.Cash;
                default:
                    throw new ArgumentOutOfRangeException(nameof(view), view, null);
            }
        }

        public TransactionViewType ToView(TransactionType entity)
        {
            switch (entity)
            {
                case TransactionType.QiwiTransfer:
                    return TransactionViewType.QiwiTransfer;
                case TransactionType.Cash:
                    return TransactionViewType.Cash;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity), entity, null);
            }
        }
    }
}