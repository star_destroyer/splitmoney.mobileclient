﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Infrastucture.Auth.Contracts;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Auth
{
    public interface IAuthorizationClient
    {
        Task<ServerInteractionInfo<BeginAuthBody>> BeginAuthorization(string phone, CancellationToken cancellation);

        Task<ServerInteractionInfo<EndAuthBody>> EndAuthorization(string phone, string serverCode, string smsCode, CancellationToken cancellation);
    }
}