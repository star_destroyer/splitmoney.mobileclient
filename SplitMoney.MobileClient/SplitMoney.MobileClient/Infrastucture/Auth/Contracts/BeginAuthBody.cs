﻿namespace SplitMoney.MobileClient.Infrastucture.Auth.Contracts
{
    public class BeginAuthBody
    {
        public string Code { get; set; }
    }
}