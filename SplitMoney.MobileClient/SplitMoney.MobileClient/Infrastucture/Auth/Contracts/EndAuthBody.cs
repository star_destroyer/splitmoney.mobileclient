﻿namespace SplitMoney.MobileClient.Infrastucture.Auth.Contracts
{
    public class EndAuthBody
    {
        public bool IsSuccess { get; set; }
        
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }
}