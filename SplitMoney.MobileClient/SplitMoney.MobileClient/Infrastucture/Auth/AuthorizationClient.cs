using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Infrastucture.Auth.Contracts;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Auth
{
    public class AuthorizationClient : IAuthorizationClient
    {
        private readonly IClientSender sender;
        
        public AuthorizationClient(IClientSender sender)
        {
            this.sender = sender;
        }

        public Task<ServerInteractionInfo<BeginAuthBody>> BeginAuthorization(string phone, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<BeginAuthBody>(GetBeginAuthorizationRequest(phone), cancellation);
        }

        public Task<ServerInteractionInfo<EndAuthBody>> EndAuthorization(string phone, string serverCode, string smsCode, CancellationToken cancellation)
        {
            cancellation.ThrowIfCancellationRequested();
            return sender.SendAsync<EndAuthBody>(GetEndAuthorizationRequest(phone, serverCode, smsCode), cancellation);
        }

        private static HttpRequestMessage GetBeginAuthorizationRequest(string phone)
        {
            return new RequestBuilder(HttpMethod.Post, "/auth/authenticate")
                .WithContent(new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("phone", phone)
                }))
                .Build();
        }

        private static HttpRequestMessage GetEndAuthorizationRequest(string phone, string code, string sms)
        {
            return new RequestBuilder(HttpMethod.Post, "/auth/obtainToken")
                .WithContent(new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("phone", phone),
                    new KeyValuePair<string, string>("code", code),
                    new KeyValuePair<string, string>("smsCode", sms)
                }))
                .Build();
        }
    }
}