﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Auth
{
    public class AuthorizationStorage : IAuthorizationStorage
    {
        private AuthorizationInfo Info { get; set; }

        public Task<AuthorizationInfo> Get(CancellationToken cancellation = default(CancellationToken))
        {
            return Task.FromResult(Info);
        }

        public Task<AuthorizationInfo> Set(AuthorizationInfo info, CancellationToken cancellation = default(CancellationToken))
        {
            Info = info;
            return Task.FromResult(Info);
        }
    }
}