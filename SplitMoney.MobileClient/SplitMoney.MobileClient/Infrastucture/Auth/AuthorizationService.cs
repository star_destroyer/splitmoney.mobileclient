﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Auth.Contracts;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;

namespace SplitMoney.MobileClient.Infrastucture.Auth
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly IAuthorizationStorage authorizationStorage;
        private readonly IAuthorizationClient authorizationClient;

        public AuthorizationService(IAuthorizationStorage authorizationStorage, IAuthorizationClient authorizationClient)
        {
            this.authorizationStorage = authorizationStorage;
            this.authorizationClient = authorizationClient;
        }

        public async Task<ServerInteractionInfo<BeginAuthBody>> BeginAuthorization(string phone, CancellationToken cancellation)
        {
            var authInfo = new AuthorizationInfo {Phone = phone};
            var info = await authorizationClient.BeginAuthorization(authInfo.Phone, cancellation);
            if (info.IsSuccess)
            {
                authInfo.UsedServerCode = info.Body.Code;
                await authorizationStorage.Set(authInfo, cancellation);
            }
            return info;
        }

        public async Task<ServerInteractionInfo<EndAuthBody>> EndAuthorization(string smsCode, CancellationToken cancellation)
        {
            var authInfo = await authorizationStorage.Get(cancellation);
            var info = await authorizationClient.EndAuthorization(authInfo.Phone, authInfo.UsedServerCode, smsCode, cancellation);
            if (info.IsSuccess && info.Body.IsSuccess)
            {
                authInfo.UsedSmsCode = smsCode;
                authInfo.AccessToken = info.Body.Token;
                authInfo.RefreshToken = info.Body.RefreshToken;
                authInfo.Name = "Пользователь SplitMoney";
                await authorizationStorage.Set(authInfo, cancellation);
            }
            return info;
        }
    }
}