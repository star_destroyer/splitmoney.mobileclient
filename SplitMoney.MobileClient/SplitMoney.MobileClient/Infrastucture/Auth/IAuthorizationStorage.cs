﻿using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.Infrastucture.Auth
{
    public interface IAuthorizationStorage
    {
        Task<AuthorizationInfo> Get(CancellationToken cancellation = default(CancellationToken));

        Task<AuthorizationInfo> Set(AuthorizationInfo info, CancellationToken cancellation = default(CancellationToken));
    }
}