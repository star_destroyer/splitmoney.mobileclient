﻿using System;
using SplitMoney.MobileClient.Domain.Enums;

namespace SplitMoney.MobileClient.Domain
{
	public class Transaction
	{
		public Guid Id { set; get; }
		public long From { get; set; }
		public DateTime Created { get; set; }
		public DateTime? Finished { get; set; }
		public Guid ConversationId { get; set; }
		public TransactionType Type { get; set; }
		public decimal Amount { get; set; }
		public Currency Currency { get; set; }
		public TransactionStatus Status { get; set; }
		public string Message { get; set; }
	}
}
