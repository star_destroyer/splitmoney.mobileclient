﻿using System;
using System.Collections.Generic;
using SplitMoney.MobileClient.Domain.Enums;

namespace SplitMoney.MobileClient.Domain
{
    public class Conversation
    {
        public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime LastEvent { get; set; }
	    public decimal Balance { get; set; }
		public int Notifications { get; set; }
	    public ConversationType Type { get; set; }
	    public IEnumerable<User> Participants { get; set; }
    }
}
