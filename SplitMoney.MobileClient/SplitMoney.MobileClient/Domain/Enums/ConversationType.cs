﻿namespace SplitMoney.MobileClient.Domain.Enums
{
    public enum ConversationType
    {
        Dialog,
        Group
    }
}