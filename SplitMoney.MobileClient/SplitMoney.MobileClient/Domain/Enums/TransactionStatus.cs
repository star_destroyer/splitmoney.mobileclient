﻿namespace SplitMoney.MobileClient.Domain.Enums
{
	public enum TransactionStatus
	{
		Pending = 0,
		Confirmed = 1,
		Rejected = 2
	}
}