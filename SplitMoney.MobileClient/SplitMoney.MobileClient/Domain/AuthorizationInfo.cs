﻿namespace SplitMoney.MobileClient.Domain
{
    public class AuthorizationInfo
    {
        public string Phone { get; set; }

        public string Name { get; set; } = "Пользователь SplitMoney";

        public string UsedServerCode { get; set; }

        public string UsedSmsCode { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}