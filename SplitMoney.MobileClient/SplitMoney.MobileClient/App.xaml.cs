﻿using Autofac;
using SplitMoney.MobileClient.Pages.Auth;
using Xamarin.Forms;

namespace SplitMoney.MobileClient
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<ContainerConfigurationModule>();
            AppContainer.Container = containerBuilder.Build();

            MainPage = new NavigationPage(new AuthPhonePage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}