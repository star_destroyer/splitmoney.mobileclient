﻿using Autofac;
using SplitMoney.MobileClient.Pages.Auth;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Transactions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Transactions
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddTransactionPage : ContentPage, IWithViewModel
    {
        public AddTransactionPage()
        {
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<AddTransactionViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }
    }
}