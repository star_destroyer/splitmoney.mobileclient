﻿using Autofac;
using SplitMoney.MobileClient.Pages.Auth;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Transactions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Transactions
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TransactionsPage : ContentPage, IWithViewModel
    {
        public TransactionsPage()
        {
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<TransactionsViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.Refresh();
        }
    }
}