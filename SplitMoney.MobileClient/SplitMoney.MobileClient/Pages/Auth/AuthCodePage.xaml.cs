﻿using Autofac;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthCodePage : ContentPage, IWithViewModel
    {
        public AuthCodePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<AuthCodeViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }
    }
}