﻿using Autofac;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Auth;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthPhonePage : ContentPage, IWithViewModel
    {
        public AuthPhonePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<AuthPhoneViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }
    }
}