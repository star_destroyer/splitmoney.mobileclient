﻿using Autofac;
using SplitMoney.MobileClient.Pages.Auth;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Conversations;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Conversations
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddContactPage : ContentPage, IWithViewModel
    {
        public AddContactPage()
        {
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<AddContactViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }
    }
}