﻿using Autofac;
using SplitMoney.MobileClient.Pages.Auth;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Conversations;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Conversations
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConversationsPage : ContentPage, IWithViewModel
    {
        public ConversationsPage()
        {
            InitializeComponent();
            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<ConversationsViewModel>();
            }

            BindingContext = ViewModel;
        }

        public ViewModelBase ViewModel { get; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel.Refresh();
        }
    }
}