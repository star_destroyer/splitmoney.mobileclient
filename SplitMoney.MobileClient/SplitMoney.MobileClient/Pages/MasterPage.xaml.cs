﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : MasterDetailPage
    {
        public MasterPage(Page page)
        {
            var menu = new MenuPage();
            Master = menu;
            menu.ViewModel.Refresh();
            Detail = new NavigationPage(page);
        }
    }
}