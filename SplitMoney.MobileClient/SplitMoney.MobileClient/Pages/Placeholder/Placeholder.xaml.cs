﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages.Placeholder
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Placeholder : ContentPage
    {
        public Placeholder()
        {
            InitializeComponent();
        }
    }
}