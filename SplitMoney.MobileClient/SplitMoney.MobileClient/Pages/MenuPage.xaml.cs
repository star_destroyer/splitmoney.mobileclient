﻿using Autofac;
using SplitMoney.MobileClient.ViewModels;
using SplitMoney.MobileClient.ViewModels.Menu;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SplitMoney.MobileClient.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();

            using (AppContainer.Container.BeginLifetimeScope())
            {
                ViewModel = AppContainer.Container.Resolve<MenuViewModel>();
            }

            BindingContext = ViewModel;
        }

        public MenuViewModel ViewModel { get; }
    }
}