﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ValueConvertes
{
    public class DecimalValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is decimal)
            {
                var money = (decimal) value;
                return money == 0 ? "0" : money.ToString(@"C", new CultureInfo("ru-RU"));
            }
            else
            {
                var money = (decimal?)value;
                return money.HasValue ? money.Value.ToString(@"C", new CultureInfo("ru-RU")) : "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return decimal.Parse(value as string ?? "0", new CultureInfo("ru-RU"));
        }
    }
}