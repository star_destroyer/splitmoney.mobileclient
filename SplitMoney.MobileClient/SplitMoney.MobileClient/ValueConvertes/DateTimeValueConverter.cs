﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ValueConvertes
{
    public class DateTimeValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var date = (DateTime) value;
            var dateDifference = DateTime.Now - date;
            if (dateDifference <= TimeSpan.FromDays(1))
            {
                return $"{date.Hour}:{date.Minute}";
            }
            else if (dateDifference <= TimeSpan.FromDays(2))
            {
                return $"Вчера";
            }
            else
            {
                return $"{date.Day} {date.ToMonthName()}";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public static class DateTimeExtensions
    {
        public static string ToMonthName(this DateTime dateTime)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
        }
    }
}