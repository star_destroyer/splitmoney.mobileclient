﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ValueConvertes
{
    public class ForHumanValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.GetForHuman();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value as string).GetByHuman();
        }
    }

    [AttributeUsage(AttributeTargets.All)]
    public class ForHumanAttribute : Attribute
    {
        public string Description { get; }

        public ForHumanAttribute(string description)
        {
            Description = description;
        }
    }

    public static class ForHumanExtensions
    {
        private static readonly Dictionary<string, object> CachedTypes = new Dictionary<string, object>();

        public static string GetForHuman(this object value)
        {
            var forHumanAttribute = value.GetType().GetTypeInfo().GetDeclaredField(value.ToString()).GetCustomAttribute<ForHumanAttribute>();
            CachedTypes[forHumanAttribute.Description] = value;
            return forHumanAttribute.Description;
        }

        public static object GetByHuman(this string forHuman)
        {
            return CachedTypes[forHuman];
        }
    }
}