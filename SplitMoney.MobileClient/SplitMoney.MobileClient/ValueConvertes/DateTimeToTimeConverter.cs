﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ValueConvertes
{
    public class DateTimeToTimeConverter : IValueConverter

    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dateTime = (DateTime) value;
            return $"{dateTime.Hour:00}:{dateTime.Minute:00}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}