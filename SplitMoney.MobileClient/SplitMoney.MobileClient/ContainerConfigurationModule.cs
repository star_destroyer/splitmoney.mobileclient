﻿using Autofac;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Conversations;
using SplitMoney.MobileClient.Infrastucture.Converters;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Transactions;
using SplitMoney.MobileClient.ViewModels.Auth;
using SplitMoney.MobileClient.ViewModels.Conversations;
using SplitMoney.MobileClient.ViewModels.Menu;
using SplitMoney.MobileClient.ViewModels.Transactions;

namespace SplitMoney.MobileClient
{
    public class ContainerConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<AuthPhoneViewModel>();
            builder.RegisterType<AuthCodeViewModel>();
            builder.RegisterType<MenuViewModel>();
            builder.RegisterType<ConversationsViewModel>();
            builder.RegisterType<AddContactViewModel>();
            builder.RegisterType<TransactionsViewModel>();
            builder.RegisterType<AddTransactionViewModel>();

            builder.RegisterType<ClientSender>().As<IClientSender>().SingleInstance();
            builder.RegisterType<ApiUriProvider>().As<IApiUriProvider>().SingleInstance();
            builder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();

            builder.RegisterType<AuthorizationService>().As<IAuthorizationService>().SingleInstance();
            builder.RegisterType<AuthorizationStorage>().As<IAuthorizationStorage>().SingleInstance();
            builder.RegisterType<AuthorizationClient>().As<IAuthorizationClient>().SingleInstance();


            builder.RegisterType<ConversationService>().As<IConversationService>().SingleInstance();
            builder.RegisterType<ConversationStorage>().As<IConversationStorage>().SingleInstance();
            builder.RegisterType<ConversationClient>().As<IConversationClient>().SingleInstance();

            builder.RegisterType<TransactionService>().As<ITransactionService>().SingleInstance();
            builder.RegisterType<TransactionStorage>().As<ITransactionStorage>().SingleInstance();
            builder.RegisterType<TransactionClient>().As<ITransactionClient>().SingleInstance();

            builder.RegisterType<ConversationConverter>().As<IConverter<ConversationView, Conversation>>().SingleInstance();
            builder.RegisterType<TransactionConverter>().As<IConverter<TransactionView, Transaction>>().SingleInstance();
            builder.RegisterType<TransactionStatusConverter>().As<IConverter<TransactionViewStatus, TransactionStatus>>().SingleInstance();
            builder.RegisterType<TransactionTypeConverter>().As<IConverter<TransactionViewType, TransactionType>>().SingleInstance();
        }
    }
}