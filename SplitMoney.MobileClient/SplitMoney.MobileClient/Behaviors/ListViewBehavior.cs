﻿using System.Windows.Input;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.Behaviors
{
    public class ListViewBehavior : BindableBehavior<ListView>
    {
        public static readonly BindableProperty ItemSelectedCommandProperty = BindableProperty.Create<ListViewBehavior, ICommand>(p => p.ItemSelectedCommand, null);

        public ICommand ItemSelectedCommand
        {
            get { return (ICommand)GetValue(ItemSelectedCommandProperty); }
            set { SetValue(ItemSelectedCommandProperty, value); }
        }

        protected override void OnAttachedTo(ListView bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.ItemSelected += OnItemSelected;
        }

        protected virtual void OnItemSelected(object sender, SelectedItemChangedEventArgs selectedItemChangedEventArgs)
        {
            var listView = sender as ListView;
            var item = listView?.SelectedItem;
            if (item != null)
            {
                listView.SelectedItem = null;
                ItemSelectedCommand?.Execute(item);
            }
        }

        protected override void OnDetachingFrom(ListView bindable)
        {
            bindable.ItemSelected -= OnItemSelected;
            base.OnDetachingFrom(bindable);
        }
    }
}