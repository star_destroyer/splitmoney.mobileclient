﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.Behaviors
{
    public class PhoneEntryBehavior: Behavior<Entry>
    {
        private const string ProviderGroup = "provider";
        private const string FirstGroup = "first";
        private const string SecondGroup = "second";
        private const string ThirdGroup = "third";

        private static readonly Regex OnlyStartFromSeven = new Regex(@"^7\s*$");

        private static readonly Regex PhoneMask = new Regex($@"^7(?<{ProviderGroup}>\d{{0,3}})(?<{FirstGroup}>\d{{0,3}})(?<{SecondGroup}>\d{{0,2}})(?<{ThirdGroup}>\d{{0,2}})",
                                                            RegexOptions.IgnoreCase);

        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            AttachEvent(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            DetachEvent(bindable);
            base.OnDetachingFrom(bindable);
        }

        private static void EntryOnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            var entry = sender as Entry;
            if (entry == null)
            {
                return;
            }

            entry.Text = GetMaskedText(textChangedEventArgs.NewTextValue);
        }

        private static void EntryOnFocused(object sender, FocusEventArgs focusEventArgs)
        {
            var entry = sender as Entry;
            if (entry == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(entry.Text))
            {
                entry.Text = "7";
            }
        }

        private static void EntryOnCompleted(object sender, EventArgs eventArgs)
        {
            var entry = sender as Entry;
            if (entry == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(entry.Text) || OnlyStartFromSeven.IsMatch(entry.Text))
            {
                entry.Text = null;
            }
        }


        private static string GetMaskedText(string currentValue)
        {
            if (string.IsNullOrEmpty(currentValue))
            {
                return null;
            }
            var digitValue = currentValue.GetOnlyDigits();
            if (string.IsNullOrEmpty(digitValue))
            {
                return null;
            }

            if (digitValue[0] != '7')
            {
                digitValue = $"7{digitValue}";
            }

            return ApplyMask(digitValue);
        }

        private static string ApplyMask(string digitValue)
        {
            var match = PhoneMask.Match(digitValue);

            var builder = new StringBuilder("7");
            if (match.Groups[ProviderGroup].Value != "")
            {
                builder.Append($" {match.Groups[ProviderGroup].Value}");
            }
            if (match.Groups[FirstGroup].Value != "")
            {
                builder.Append($" {match.Groups[FirstGroup].Value}");
            }
            if (match.Groups[SecondGroup].Value != "")
            {
                builder.Append($" {match.Groups[SecondGroup].Value}");
            }
            if (match.Groups[ThirdGroup].Value != "")
            {
                builder.Append($" {match.Groups[ThirdGroup].Value}");
            }
            return builder.ToString();
        }

        private static void AttachEvent(Entry entry)
        {
            entry.TextChanged += EntryOnTextChanged;
            entry.Focused += EntryOnFocused;
            entry.Completed += EntryOnCompleted;
        }

        private static void DetachEvent(Entry entry)
        {
            entry.TextChanged -= EntryOnTextChanged;
            entry.Focused -= EntryOnFocused;
            entry.Completed -= EntryOnCompleted;
        }
    }
}