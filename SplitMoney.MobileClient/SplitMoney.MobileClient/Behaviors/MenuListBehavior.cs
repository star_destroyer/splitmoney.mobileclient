﻿using SplitMoney.MobileClient.Pages;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.Behaviors
{
    public class MenuListBehavior : ListViewBehavior
    {

        protected override void OnItemSelected(object sender, SelectedItemChangedEventArgs selectedItemChangedEventArgs)
        {
            var masterPage = Application.Current.MainPage as MasterPage;
            if (masterPage != null)
            {
                masterPage.IsPresented = false;
            }
            base.OnItemSelected(sender, selectedItemChangedEventArgs);
        }
    }
}