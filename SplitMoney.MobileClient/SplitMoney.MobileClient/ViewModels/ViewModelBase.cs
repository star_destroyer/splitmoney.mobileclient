﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Infrastucture.Core.Validation;
using SplitMoney.MobileClient.ViewModels.Auth;

namespace SplitMoney.MobileClient.ViewModels
{
    public abstract class ViewModelBase : Notificatable
    {
        private readonly IDictionary<string, object> properties = new Dictionary<string, object>();

        protected Validatable<TValue> GetValidatable<TValue>(Expression<Func<TValue>> path)
        {
            var propertyName = path.GetPropertyName();
            return (Validatable<TValue>) properties[propertyName];
        }

        protected void SetValidatable<TValue>(TValue value, [CallerMemberName] string property = null)
        {
            if (property == null)
            {
                throw new NullReferenceException();
            }

            var validatable = properties[property] as Validatable<TValue>;
            validatable.Value = value;
            OnPropertyChanged(property);
        }

        protected ValidationBuilder<TValue> RegisterValidatable<TValue>(Expression<Func<Validatable<TValue>>> path)
        {
            var propertyName = path.GetPropertyName();
            var validatable = new Validatable<TValue>();
            Set(validatable, propertyName);
            return new ValidationBuilder<TValue>(validatable);
        }


        public abstract Task Init(params object[] parameters);

        public abstract Task Refresh(params object[] parameters);
    }
}