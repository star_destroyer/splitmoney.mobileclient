﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Properties;

namespace SplitMoney.MobileClient.ViewModels
{
    public abstract class Notificatable : INotifyPropertyChanged
    {
        private readonly IDictionary<string, object> properties = new Dictionary<string, object>();

        protected TValue Get<TValue>(TValue deafult = default(TValue), [CallerMemberName] string property = null)
        {
            if (property == null)
            {
                throw new NullReferenceException();
            }
            return properties.ContainsKey(property) ? (TValue)properties[property] : deafult;
        }

        protected void Set(object value, [CallerMemberName] string property = null)
        {
            if (property == null)
            {
                throw new NullReferenceException();
            }
            properties[property] = value;
            OnPropertyChanged(property);
        }

        public void RegisterHook<TValue>(Expression<Func<TValue>> path, Action action)
        {
            var propertyName = path.GetPropertyName();
            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName.Equals(propertyName))
                {
                    action.Invoke();
                }
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}