using System;

namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public class TransactionView : Notificatable
    {
        public Guid Id { get; set; }

        public Guid ConversationId { get; set; }

        public bool IsMy
        {
            get => Get<bool>();
            set => Set(value);
        }

        public string From
        {
            get => Get<string>();
            set => Set(value);
        }

        public decimal Amount
        {
            get => Get<decimal>();
            set => Set(value);
        }

        public string Comment
        {
            get => Get<string>();
            set => Set(value);
        }

        public TransactionViewType Type
        {
            get => Get<TransactionViewType>();
            set => Set(value);
        }

        public TransactionViewStatus Status
        {
            get => Get(TransactionViewStatus.Init);
            set => Set(value);
        }

        public DateTime Time
        {
            get => Get<DateTime>();
            set => Set(value);
        }
    }
}