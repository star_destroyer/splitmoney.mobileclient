namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public enum TransactionViewStatus
    {
        Init = 0,
        Sended = 1,
        Success = 2,
        Rejected = 3,
        Error = 4
    }
}