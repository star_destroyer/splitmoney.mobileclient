﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Domain.Enums;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Conversations;
using SplitMoney.MobileClient.Infrastucture.Converters;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Infrastucture.Core.Validation;
using SplitMoney.MobileClient.Infrastucture.Transactions;
using SplitMoney.MobileClient.ValueConvertes;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public class AddTransactionViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;
        private readonly ITransactionService transactionService;
        private readonly ITransactionStorage transactionStorage;
        private readonly IAuthorizationStorage authorizationStorage;
        private readonly IConversationStorage conversationStorage;
        private readonly IConverter<TransactionViewType, TransactionType> typeConverter;
        private readonly IConverter<TransactionViewStatus, TransactionStatus> statusConverter;

        public AddTransactionViewModel(INavigationService navigationService,
                                       ITransactionService transactionService,
                                       ITransactionStorage transactionStorage,
                                       IAuthorizationStorage authorizationStorage,
                                       IConversationStorage conversationStorage,
                                       IConverter<TransactionViewType, TransactionType> typeConverter,
                                       IConverter<TransactionViewStatus, TransactionStatus> statusConverter)
        {
            this.navigationService = navigationService;
            this.transactionService = transactionService;
            this.transactionStorage = transactionStorage;
            this.authorizationStorage = authorizationStorage;
            this.conversationStorage = conversationStorage;
            this.typeConverter = typeConverter;
            this.statusConverter = statusConverter;

            RegisterValidatable(() => AmountInput)
                .AddRule(() => string.IsNullOrWhiteSpace(AmountInput.Value), "Введите сумму")
                .AddRule(() => !decimal.TryParse(AmountInput.Value, out decimal dummy), "Не верный формат ввода");

            RegisterValidatable(() => Comment)
                .AddRule(() => !string.IsNullOrWhiteSpace(Comment.Value) && Comment.Value.Length > 70, "Комментарий слишком длинный. Уменьшите его до 70 символов");

            RegisterValidatable(() => Code)
                .AddRule(() => string.IsNullOrWhiteSpace(Code.Value), "Введите проверочный код");
        }

        public Guid Id { get; set; }

        public Guid ConversationId { get; set; }

        public bool IsMy
        {
            get => Get<bool>();
            set
            {
                Set(value);
                IsSendButtonEnabled = value;
            }
        }

        
        public string Avatar
        {
            get => Get(AvatarRandomizer.Get());
            set => Set(value);
        }

        public string Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Phone
        {
            get => Get<string>();
            set => Set(value);
        }

        public Validatable<string> AmountInput
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public Validatable<string> Comment
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public Validatable<string> Code
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public TransactionViewType Type
        {
            get => Get(TransactionViewType.QiwiTransfer);
            set => Set(value);
        }

        public TransactionViewStatus Status
        {
            get => Get(TransactionViewStatus.Init);
            set => Set(value);
        }

        public bool IsSendButtonEnabled
        {
            get => Get(true);
            set => Set(value);
        }

        public string[] AvailibleTransactionType => new[]
            {TransactionViewType.QiwiTransfer.GetForHuman(), TransactionViewType.Cash.GetForHuman()};

        public ICommand SendTransactionCommand => new Command(SendTransactionCallback);

        public ICommand RetryCommand => new Command(RetryCallback);

        public ICommand BackCommand => new Command(BackCallback);

        public ICommand SendCodeCommand => new Command(SendCodeCallback);

        private async void SendTransactionCallback()
        {
            var commentIsValid = Comment.CheckValidation();
            var amountIsValid = AmountInput.CheckValidation();
            if (!commentIsValid || !amountIsValid)
            {
                return;
            }
            IsSendButtonEnabled = false;
            switch (Type)
            {
                case TransactionViewType.QiwiTransfer:
                     await SendQiwiTransaction();
                    break;
                case TransactionViewType.Cash:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
            IsSendButtonEnabled = true;
        }

        private async Task SendQiwiTransaction()
        {
            var transaction = await transactionService.CreateTransaction(ConversationId, decimal.Parse(AmountInput.Value), CancellationToken.None);
            if (transaction == null)
            {
                Status = TransactionViewStatus.Error;
                return;
            }

            Id = transaction.Id;
            Status = TransactionViewStatus.Sended;
        }

        private async void SendCodeCallback()
        {
            if (!Code.CheckValidation())
            {
                return;
            }
            IsSendButtonEnabled = false;
            var info = await transactionService.ConfirmTransaction(Id, Code.Value, CancellationToken.None);
            switch (info.Result)
            {
                case ServerInteractionResult.Success:
                    Status = TransactionViewStatus.Success;
                    break;
                case ServerInteractionResult.NoConnection:    
                case ServerInteractionResult.ServerError:
                    Status = TransactionViewStatus.Error;
                    break;
                case ServerInteractionResult.BadParameters:
                    Code.Invalidate("Введен не верный проверочный код");
                    break;
            }
            IsSendButtonEnabled = true;
        }


        private async void RetryCallback()
        {
            IsSendButtonEnabled = false;
            await SendQiwiTransaction();
            IsSendButtonEnabled = true;
        }

        private async void BackCallback()
        {
            await navigationService.BackInMaster(ConversationId);
        }

        public override async Task Init(params object[] parameters)
        {
            await Refresh(parameters);
        }

        public override async Task Refresh(params object[] parameters)
        {
            var transactionId = Id;
            var conversationId = ConversationId;
            if (parameters.Length > 0)
            {
                conversationId = (Guid) parameters[0];
            }
            if (parameters.Length > 1)
            {
                transactionId = (Guid) parameters[1];
            }
            await Refresh(conversationId, transactionId);
        }

        private async Task Refresh(Guid conversationId)
        {
            if (conversationId.Equals(Guid.Empty))
            {
                return;
            }
            var info = await authorizationStorage.Get(CancellationToken.None);
            var conversation = await conversationStorage.Get(conversationId, CancellationToken.None);
            Name = conversation.Name;
            Phone = conversation.Participants.First(x => !x.Id.ToString().Equals(info.Phone)).Id.ToString();
            ConversationId = conversationId;
        }

        private async Task Refresh(Guid conversationId, Guid transactionId)
        {
            await Refresh(conversationId);
            if (transactionId.Equals(Guid.Empty))
            {
                return;
            }
            var info = await authorizationStorage.Get(CancellationToken.None);
            var transaction = await transactionStorage.Get(transactionId);
            AmountInput.Value = transaction.Amount.ToString();
            Comment.Value = transaction.Message;
            Status = statusConverter.ToView(transaction.Status);
            Type = typeConverter.ToView(transaction.Type);
            IsMy = info.Phone.Equals(transaction.From.ToString());
        }
    }
}