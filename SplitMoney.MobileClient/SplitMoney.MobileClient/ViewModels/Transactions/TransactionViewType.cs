﻿using SplitMoney.MobileClient.ValueConvertes;

namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public enum TransactionViewType
    {
        [ForHuman("Qiwi Кошелек")]QiwiTransfer,
        [ForHuman("Наличные")] Cash
    }
}