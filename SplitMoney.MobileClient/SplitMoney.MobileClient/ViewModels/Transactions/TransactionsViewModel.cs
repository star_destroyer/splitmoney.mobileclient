﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Conversations;
using SplitMoney.MobileClient.Infrastucture.Converters;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Transactions;
using SplitMoney.MobileClient.Pages.Transactions;
using SplitMoney.MobileClient.ValueConvertes;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public class TransactionsViewModel : ViewModelBase
    {
        private readonly INavigationService navigationService;
        private readonly ITransactionService transactionService;
        private readonly ITransactionStorage transactionStorage;
        private readonly IConversationStorage conversationStorage;
        private readonly IConverter<TransactionView, Transaction> converter;

        public TransactionsViewModel(INavigationService navigationService,
                                     ITransactionService transactionService,
                                     ITransactionStorage transactionStorage,
                                     IConversationStorage conversationStorage,
                                     IConverter<TransactionView, Transaction> converter)
        {
            this.navigationService = navigationService;
            this.transactionService = transactionService;
            this.transactionStorage = transactionStorage;
            this.conversationStorage = conversationStorage;
            this.converter = converter;
        }

        public Guid ConversationId { get; set; }
        
        public string Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public ObservableCollection<TransactionGroupItem> Items
        {
            get => Get<ObservableCollection<TransactionGroupItem>>();
            set => Set(value);
        }

        public bool IsRefreshing
        {
            get => Get(false);
            set => Set(value);
        }

        public ICommand AddNewTransactionCommand => new Command(AddNewTransactionCallback);

        public ICommand RefreshCommand => new Command(RefreshCallback);

        public ICommand ItemSelectedCommand => new Command(ItemSelectedCallback);

        private async void ItemSelectedCallback(object obj)
        {
            var transactionView = obj as TransactionView;
            if (transactionView != null)
            {
                await navigationService.GoToInMasterStack(new AddTransactionPage(), ConversationId, transactionView.Id);
            }
        }

        private async void AddNewTransactionCallback()
        {
            await navigationService.GoToInMasterStack(new AddTransactionPage(), ConversationId);
        }

        private async void RefreshCallback(object a)
        {
            await SyncTransactions(ConversationId);
            IsRefreshing = false;
        }


        public override async Task Init(params object[] parameters)
        {
            if (parameters.Length == 0 || !(parameters[0] is Guid))
            {
                return;
            }
            var conversationId = (Guid) parameters[0];
            await SyncTransactions(conversationId);
        }

        private async Task SyncTransactions(Guid conversationId)
        {
            if (await transactionService.RefreshTransactions(conversationId, CancellationToken.None))
            {
                await Refresh(conversationId);
            }
        }

        public override async Task Refresh(params object[] parameters)
        {
            var conversationId = ConversationId;
            if (parameters.Length > 0)
            {
                conversationId = (Guid) parameters[0];
            }
            if (!conversationId.Equals(Guid.Empty))
            {
                await Refresh(conversationId);
            }
        }

        private async Task Refresh(Guid conversationId)
        {
            var transactions = await transactionStorage.Select();
            var conversation = await conversationStorage.Get(conversationId, CancellationToken.None);
            ConversationId = conversationId;
            var groupedTransations = transactions.GroupBy(x => GetKey(x.Created)).Select(x => new TransactionGroupItem(x.Key, x.Select(converter.ToView)));
            Items = new ObservableCollection<TransactionGroupItem>(groupedTransations);
            Name = conversation.Name;
        }

        private static string GetKey(DateTime time)
        {
            return $"{time.Day} {time.ToMonthName()}";
        }
    }
}