﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SplitMoney.MobileClient.ViewModels.Transactions
{
    public class TransactionGroupItem : ObservableCollection<TransactionView>
    {
        public TransactionGroupItem(string groupName, IEnumerable<TransactionView> chatItems)
        {
            GroupName = groupName;
            foreach (var chatItem in chatItems)
            {
                Add(chatItem);
            }
        }

        public string GroupName { get; }
    }
}