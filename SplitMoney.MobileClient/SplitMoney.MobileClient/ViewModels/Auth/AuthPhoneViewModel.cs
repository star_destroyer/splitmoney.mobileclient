﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Infrastucture.Core.Validation;
using SplitMoney.MobileClient.Pages;
using SplitMoney.MobileClient.Pages.Auth;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Auth
{
    public class AuthPhoneViewModel : ViewModelBase
    {
        private readonly IAuthorizationService authorizationService;
        private readonly IAuthorizationStorage authorizationStorage;
        private readonly INavigationService navigationService;

        public AuthPhoneViewModel(IAuthorizationService authorizationService, IAuthorizationStorage authorizationStorage, INavigationService navigationService)
        {
            this.authorizationService = authorizationService;
            this.authorizationStorage = authorizationStorage;
            this.navigationService = navigationService;
            RegisterValidatable(() => Phone)
                .AddRule(() => string.IsNullOrEmpty(Phone.Value), "Введите номер телефона")
                .AddRule(() => Phone.Value[0] != '7', "Телефон должен начинаться с цифры 7 без знака +")
                .AddRule(() => Phone.Value.GetOnlyDigits().Length != 11, "Телефон должен содержать 11 цифр");
        }

        public Validatable<string> Phone
        {
            get => Get<Validatable<string>>();
            set => SetValidatable(value);
        }

        public bool IsSendButtonEnabled
        {
            get => Get(true);
            set => Set(value);
        }

        public Command SendPhoneCommand => new Command(SendPhoneCallback);

        private async void SendPhoneCallback()
        {
            if (!Phone.CheckValidation())
            {
                return;
            }
            IsSendButtonEnabled = false;
            var authResult = await authorizationService.BeginAuthorization(Phone.Value.GetOnlyDigits(), CancellationToken.None);
            if (authResult.IsSuccess)
            {
                await navigationService.GoTo(new AuthCodePage());
            }
            else
            {
                Phone.Invalidate(GetValidationMessage(authResult.Result));
            }
            IsSendButtonEnabled = true;
        }


        private static string GetValidationMessage(ServerInteractionResult interactionResult)
        {
            switch (interactionResult)
            {
                case ServerInteractionResult.NoConnection:
                    return "Не могу подключится к серверу";
                case ServerInteractionResult.ServerError:
                    return "На сервере произошла ошибка, обратитесь в техподдержку";
                case ServerInteractionResult.BadParameters:
                    return "Не можем отправить смс на этот телефон";
                case ServerInteractionResult.Unauthorized:
                    return "На сервере произошла ошибка, обратитесь в техподдержку";
                default:
                    throw new ArgumentException("Не известный код");
            }
        }

        public override async Task Init(params object[] parameters)
        {
        }

        public override async Task Refresh(params object[] parameters)
        {
            var authorizationInfo = await authorizationStorage.Get();
            Phone.Value = authorizationInfo.Phone;
        }
    }
}