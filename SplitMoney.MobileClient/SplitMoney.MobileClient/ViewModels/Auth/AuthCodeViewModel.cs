﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.ExternalInteraction;
using SplitMoney.MobileClient.Infrastucture.Core.Validation;
using SplitMoney.MobileClient.Pages;
using SplitMoney.MobileClient.Pages.Conversations;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Auth
{
    public class AuthCodeViewModel : ViewModelBase
    {
        private readonly IAuthorizationService authorizationService;
        private readonly INavigationService navigationService;

        public AuthCodeViewModel(IAuthorizationService authorizationService, INavigationService navigationService)
        {
            this.authorizationService = authorizationService;
            this.navigationService = navigationService;
            RegisterValidatable(() => Code)
                .AddRule(() => string.IsNullOrEmpty(Code.Value), "Введите проверочный код")
                .AddRule(() => Code.Value.Length != 4, "Проверочный код должен состоять из 4 цифр");
        }

        public Validatable<string> Code
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public bool IsSendButtonEnabled
        {
            get => Get(true);
            set => Set(value);
        }

        public ICommand SendCodeCommand => new Command(SendCodeCallback);

        private async void SendCodeCallback()
        {
            if (!Code.CheckValidation())
            {
                return;
            }
            IsSendButtonEnabled = false;
            var info = await authorizationService.EndAuthorization(Code.Value, CancellationToken.None);
            if (info.IsSuccess)
            {
                await navigationService.GoToInNewMasterStak(new ConversationsPage());
            }
            else
            {
                Code.Invalidate(GetValidationMessage(info.Result));
            }
            IsSendButtonEnabled = true;
        }

        private static string GetValidationMessage(ServerInteractionResult interactionResult)
        {
            switch (interactionResult)
            {
                case ServerInteractionResult.NoConnection:
                    return "Не могу подключится к серверу";
                case ServerInteractionResult.BadParameters:
                    return "Введен не верный проверочный код";
                case ServerInteractionResult.ServerError:
                    return "На сервере произошла ошибка, обратитесь в техподдержку";
                default:
                    throw new ArgumentException("Не известный код");
            }
        }

        public override async Task Init(params object[] parameters)
        {
            
        }

        public override async Task Refresh(params object[] parameters)
        {
        }
    }
}