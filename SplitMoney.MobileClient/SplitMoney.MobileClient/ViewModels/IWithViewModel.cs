﻿namespace SplitMoney.MobileClient.ViewModels
{
    public interface IWithViewModel
    {
        ViewModelBase ViewModel { get; }
    }
}