using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SplitMoney.MobileClient.ViewModels.Menu
{
    public class MenuGroupItem : ObservableCollection<MenuItem>
    {
        public MenuGroupItem(IEnumerable<MenuItem> menuItems, bool isWithHeader = true)
        {
            IsWithHeader = isWithHeader;
            foreach (var menuItem in menuItems)
            {
                Add(menuItem);
            }
        }

        public bool IsWithHeader { get; }
    }
}