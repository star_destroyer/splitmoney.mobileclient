namespace SplitMoney.MobileClient.ViewModels.Menu
{
    public enum MenuItemType
    {
        CreatePearToPear,
        CreateConversations,
        Conversations,
        Notifications,
        Settings,
        Exit
    }
}