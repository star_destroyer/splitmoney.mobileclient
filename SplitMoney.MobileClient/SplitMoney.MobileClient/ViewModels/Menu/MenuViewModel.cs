﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Pages.Auth;
using SplitMoney.MobileClient.Pages.Conversations;
using SplitMoney.MobileClient.Pages.Placeholder;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Menu
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly IAuthorizationStorage authorizationStorage;
        private readonly INavigationService navigationService;

        public MenuViewModel(IAuthorizationStorage authorizationStorage, INavigationService navigationService)
        {
            this.authorizationStorage = authorizationStorage;
            this.navigationService = navigationService;
        }

        public string Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Phone
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Avatar
        {
            get => Get(AvatarRandomizer.Get());
            set => Set(value);
        }

        public IEnumerable<MenuGroupItem> Items => new[]
        {
            new MenuGroupItem(new[]
            {
                new MenuItem(MenuItemType.CreatePearToPear) {Icon = "speech.png", Title = "Добавить разговор"},
                new MenuItem(MenuItemType.CreateConversations) {Icon = "speech.png", Title = "Добавить беседу"}
            }, false),
            new MenuGroupItem(new[]
            {
                new MenuItem(MenuItemType.Conversations) {Icon = "speech.png", Title = "Сообщения"},
                new MenuItem(MenuItemType.Notifications) {Icon = "bell.png", Title = "Уведомления"},
                new MenuItem(MenuItemType.Settings) {Icon = "settings.png", Title = "Настройки"},
                new MenuItem(MenuItemType.Exit) {Icon = "signout.png", Title = "Выход"}
            })
        };

        public ICommand ItemSelectedCommand => new Command(ItemSelectedCallback);

        private async void ItemSelectedCallback(object parameter)
        {
            var menuItem = parameter as MenuItem;
            if (menuItem == null)
            {
                throw new ArgumentNullException(nameof(menuItem), $"Параметр комманды не {nameof(MenuItem)}");
            }

            switch (menuItem.Type)
            {
                case MenuItemType.CreatePearToPear:
                    await navigationService.GoToInMasterStack(new AddContactPage());
                    break;
                case MenuItemType.CreateConversations:
                    await navigationService.GoToInMasterStack(new Placeholder());
                    break;
                case MenuItemType.Conversations:
                    await navigationService.GoToInNewMasterStak(new ConversationsPage());
                    break;
                case MenuItemType.Notifications:
                    await navigationService.GoToInNewMasterStak(new Placeholder());
                    break;
                case MenuItemType.Settings:
                    await navigationService.GoToInNewMasterStak(new Placeholder());
                    break;
                case MenuItemType.Exit:
                    await ExitCallback();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private async Task ExitCallback()
        {

            var result = await Application.Current.MainPage.DisplayAlert("Вы действительно хотите выйти?",
                                                                         "Для повторного входа вам нужно будет ввести код из смс.",
                                                                         "Да",
                                                                         "Нет");
            if (result)
            {
                await navigationService.GoToInNewStak(new AuthPhonePage());
            }
        }

        public override async Task Init(params object[] parameters)
        {
            await Refresh();
        }

        public override async Task Refresh(params object[] parameters)
        {
            var info = await authorizationStorage.Get();
            Name = info.Name;
            Phone = info.Phone;
        }
    }
}