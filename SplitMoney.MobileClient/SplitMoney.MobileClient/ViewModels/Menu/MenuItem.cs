namespace SplitMoney.MobileClient.ViewModels.Menu
{
    public class MenuItem
    {
        public MenuItem(MenuItemType type)
        {
            Type = type;
        }

        public MenuItemType Type { get; }

        public string Icon { get; set; }

        public string Title { get; set; }
   }
}