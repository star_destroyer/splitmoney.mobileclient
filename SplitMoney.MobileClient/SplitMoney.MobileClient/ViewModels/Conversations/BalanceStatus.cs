namespace SplitMoney.MobileClient.ViewModels.Conversations
{
    public enum BalanceStatus
    {
        Zero = 0,
        Bad = 1,
        Good = 2
    }
}