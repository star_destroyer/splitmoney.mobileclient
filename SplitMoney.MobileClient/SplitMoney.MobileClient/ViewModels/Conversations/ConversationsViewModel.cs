﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Domain;
using SplitMoney.MobileClient.Infrastucture.Conversations;
using SplitMoney.MobileClient.Infrastucture.Converters;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Pages;
using SplitMoney.MobileClient.Pages.Conversations;
using SplitMoney.MobileClient.Pages.Transactions;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Conversations
{
    public sealed class ConversationsViewModel : ViewModelBase
    {
        private readonly IConversationService conversationService;
        private readonly IConversationStorage conversationStorage;
        private readonly INavigationService navigationService;
        private readonly IConverter<ConversationView, Conversation> converter;

        public ConversationsViewModel(IConversationService conversationService,
                                      IConversationStorage conversationStorage,
                                      INavigationService navigationService,
                                      IConverter<ConversationView, Conversation> converter)
        {
            this.conversationService = conversationService;
            this.conversationStorage = conversationStorage;
            this.navigationService = navigationService;
            this.converter = converter;
        }

        public ObservableCollection<ConversationView> Items
        {
            get => Get(new ObservableCollection<ConversationView>());
            set
            {
                Set(value);
                IsEmpty = value.Count == 0;
            }
        }

        public bool IsRefreshing
        {
            get => Get(false);
            set => Set(value);
        }

        public bool IsEmpty
        {
            get => Get(false);
            set => Set(value);
        }


        public ICommand AddNewContactCommand => new Command(AddNewContactCallback);

        public ICommand RefreshCommand => new Command(RefreshCallback);

        public ICommand SelectItemCommand => new Command(SelectItemCallback);

        private async void SelectItemCallback(object parameter)
        {
            var conversationView = parameter as ConversationView;
            if (conversationView != null)
            {
                await navigationService.GoToInMasterStack(new TransactionsPage(), conversationView.Id);
            }
        }

        private async void RefreshCallback(object a)
        {
            await SyncConversations();
            IsRefreshing = false;
        }

        private async void AddNewContactCallback()
        {
            await navigationService.GoToInMasterStack(new AddContactPage());
        }

        public override async Task Init(params object[] parameters)
        {
            await SyncConversations();
        }

        public override async Task Refresh(params object[] parameters)
        {
            var conversations = await conversationStorage.Select(CancellationToken.None);
            Items = new ObservableCollection<ConversationView>(conversations.Select(x => converter.ToView(x)));
        }

        private async Task SyncConversations()
        {
            if (await conversationService.RefreshConversation(CancellationToken.None))
            {
                await Refresh();
            }
        }
    }
}