using System;
using System.Collections.Generic;
using SplitMoney.MobileClient.Domain;

namespace SplitMoney.MobileClient.ViewModels.Conversations
{
    public class ConversationView : Notificatable
    {
        public Guid Id { get; set; }

        public IEnumerable<User> Participants { get; set; }

        public string Icon
        {
            get => Get<string>();
            set => Set(value);
        }

        public string Name
        {
            get => Get<string>();
            set => Set(value);
        }

        public decimal Balance
        {
            get => Get<decimal>();
            set
            {
                Set(value);
                BalanceStatus = value == 0 ? BalanceStatus.Zero : (value > 0 ? BalanceStatus.Good : BalanceStatus.Bad);
            }
        }

        public DateTime LastMessageTime
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        public int NotificationCount
        {
            get => Get(0);
            set => Set(value);
        }

        public BalanceStatus BalanceStatus
        {
            get => Get<BalanceStatus>();
            set => Set(value);
        }
    }
}