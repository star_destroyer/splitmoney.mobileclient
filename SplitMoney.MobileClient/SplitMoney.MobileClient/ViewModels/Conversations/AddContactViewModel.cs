﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using SplitMoney.MobileClient.Infrastucture.Auth;
using SplitMoney.MobileClient.Infrastucture.Conversations;
using SplitMoney.MobileClient.Infrastucture.Core;
using SplitMoney.MobileClient.Infrastucture.Core.Utils;
using SplitMoney.MobileClient.Infrastucture.Core.Validation;
using SplitMoney.MobileClient.Pages;
using SplitMoney.MobileClient.ViewModels.Auth;
using Xamarin.Forms;

namespace SplitMoney.MobileClient.ViewModels.Conversations
{
    public class AddContactViewModel : ViewModelBase
    {
        private readonly IConversationService conversationService;
        private readonly IConversationStorage conversationStorage;
        private readonly INavigationService navigationService;
        private readonly IAuthorizationStorage authorizationStorage;

        public AddContactViewModel(IConversationService conversationService,
                                   IConversationStorage conversationStorage,
                                   INavigationService navigationService,
                                   IAuthorizationStorage authorizationStorage)
        {
            this.conversationService = conversationService;
            this.conversationStorage = conversationStorage;
            this.navigationService = navigationService;
            this.authorizationStorage = authorizationStorage;

            RegisterValidatable(() => Name)
                .AddRule(() => string.IsNullOrWhiteSpace(Name.Value), "Введите имя");

            RegisterValidatable(() => Phone)
                .AddRule(() => string.IsNullOrWhiteSpace(Phone.Value), "Введите номер телефона")
                .AddRule(() => Phone.Value[0] != '7', "Телефон должен начинаться с цифры 7 без знака +")
                .AddRule(() => Phone.Value.GetOnlyDigits().Length != 11, "Телефон должен содержать 11 цифр");
        }

        public Guid Id { get; set; }

        public Validatable<string> Name
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public Validatable<string> Phone
        {
            get => Get<Validatable<string>>();
            set => Set(value);
        }

        public bool IsAddButtonEnabled
        {
            get => Get<bool>(true);
            set => Set(value);
        }

        public ICommand AddCommand => new Command(AddCallback);

        private async void AddCallback()
        {
            var isNameValid = Name.CheckValidation();
            var isPhoneValid = Phone.CheckValidation();
            if (!isNameValid || !isPhoneValid)
            {
                return;
            }
            IsAddButtonEnabled = false;
            var info = await conversationService.CreateNewConversation(Phone.Value.GetOnlyDigits(), Name.Value, CancellationToken.None);
            if (info.IsSuccess)
            {
                await navigationService.BackInMaster();
            }
            else
            {
                Phone.Invalidate("Пользователь с таким телефоном не зарегистрирован в система");
            }
            IsAddButtonEnabled = true;
        }

        public override async Task Init(params object[] parameters)
        {
        }

        public override async Task Refresh(params object[] parameters)
        {
            var conversation = await conversationStorage.Get(Id, CancellationToken.None);
            var authInfo = await authorizationStorage.Get(CancellationToken.None);
            if (conversation != null)
            {
                Name.Value = conversation.Name;
                Phone.Value = conversation.Participants.First(x => !x.Id.ToString().Equals(authInfo.Phone)).Id.ToString();
            }
        }
    }
}