﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;

namespace SplitMoney.MobileClient.Droid
{
    [Activity(Label = "SplitMoney", 
        MainLauncher = true,
        Icon = "@drawable/icon",
        Theme = "@style/MainTheme",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);
            FAB.Droid.FloatingActionButtonRenderer.InitControl();
            RoundedBoxViewRenderer.Init();
            LoadApplication(new App());
        }
    }
}

