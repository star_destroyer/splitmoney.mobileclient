﻿using SplitMoney.MobileClient.iOS;
using SplitMoney.MobileClient.iOS.SplitMoney.MobileClient.iOS;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(SplitMoney.MobileClient.iOS.RoundedBoxView), typeof(RoundedBoxViewRenderer))]
namespace SplitMoney.MobileClient.iOS
{
    /// <summary>
    /// RoundedBoxView Renderer
    /// </summary>
    public class RoundedBoxViewRenderer //: TRender (replace with renderer type
    {
        /// <summary>
        /// Used for registration with dependency service
        /// </summary>
        public static void Init()
        {
        }
    }
}